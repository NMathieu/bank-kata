package com.sg.bank.kata.controller.api;

import com.sg.bank.kata.model.OperationTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@ApiModel(value="BankOperationRequest", description="Describe the bank operation done by the client")
public class BankOperationRequest {

    @ApiParam(name ="Amount")
    private final @NotNull BigDecimal amount;

    @ApiParam(name="Description", value="Details the operation")
    private final String description;

    @ApiParam(name="OperationType", value="Type of the operation", example = "DEPOSIT, WITHDRAWAL")
    private final @NotNull OperationTypeEnum operationType;

    public BankOperationRequest( BigDecimal amount,
                                 String description,
                                 OperationTypeEnum operationType) {
        this.amount = amount;
        this.description = description;
        this.operationType = operationType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getDescription() {
        return description;
    }

    public OperationTypeEnum getOperationType() {
        return operationType;
    }
}
