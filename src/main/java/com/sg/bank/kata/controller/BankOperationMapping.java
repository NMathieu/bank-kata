package com.sg.bank.kata.controller;

import com.sg.bank.kata.controller.api.BankOperationRequest;
import com.sg.bank.kata.controller.exception.BadRequestException;
import com.sg.bank.kata.model.Operation;
import com.sg.bank.kata.model.OperationTypeEnum;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class BankOperationMapping {

    public Operation mapToOperation(BankOperationRequest bankOperationRequest){
        validate(bankOperationRequest);
       return new Operation.OperationBuilder()
                .withAmount(bankOperationRequest.getAmount())
                .withDate( LocalDate.now())
                .withDescription(bankOperationRequest.getDescription())
                .withType(bankOperationRequest.getOperationType())
                .build();
    }

    private void validate(BankOperationRequest bankOperationRequest) {
        if (bankOperationRequest.getOperationType() == OperationTypeEnum.DEPOSIT
           && bankOperationRequest.getAmount().doubleValue() < 0) {
            throw new BadRequestException("Amount can't be negate");
        }
        if (bankOperationRequest.getOperationType() == OperationTypeEnum.WITHDRAWAL
                && bankOperationRequest.getAmount().doubleValue() > 0) {
            throw new BadRequestException("Amount can't be positive");
        }
    }


}
