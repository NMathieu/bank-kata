package com.sg.bank.kata.controller;

import com.sg.bank.kata.controller.api.BankOperationRequest;
import com.sg.bank.kata.model.Account;
import com.sg.bank.kata.model.Client;
import com.sg.bank.kata.model.Operation;
import com.sg.bank.kata.service.AccountService;
import com.sg.bank.kata.service.ClientService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@Api(value = "BankOperationController")
public class BankOperationController {

    private final BankOperationMapping bankOperationMapping;
    private final AccountService accountService;
    private final ClientService clientService;

    public BankOperationController(BankOperationMapping bankOperationMapping,
                                   AccountService accountService,
                                   ClientService clientService) {
        this.bankOperationMapping = bankOperationMapping;
        this.accountService = accountService;
        this.clientService = clientService;
    }

    @GetMapping("clients")
    public List<Client> getClients() {
        return clientService.findAllClients();
    }

    @GetMapping("clients/{clientId}/accounts/")
    public List<Account> getAccounts(@PathVariable long clientId) {
        return accountService.findAllAccountsByClientId(clientId);
    }

    @GetMapping("clients/{clientId}/accounts/{accountCode}")
    public Account getAccount(@PathVariable long clientId,
                              @PathVariable String accountCode) {
        return accountService.findAllAccountsByClientIdAndAccountCode(clientId, accountCode);
    }

    @ApiOperation(value = "To make a deposit on a bank account")
    @PostMapping("clients/{clientId}/accounts/{accountCode}/operations")
    public void makeAnOperation(@PathVariable long clientId,
                                @PathVariable String accountCode,
                                @RequestBody BankOperationRequest bankOperationRequest) {
        Operation operation = bankOperationMapping.mapToOperation(bankOperationRequest);
        accountService.saveOperation(clientId, accountCode, operation);
    }
}
