package com.sg.bank.kata.service;

import com.sg.bank.kata.model.Client;
import com.sg.bank.kata.repository.ClientRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClientService {
    private final ClientRepository clientRepository;

    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public List<Client> findAllClients() {
        return clientRepository.findAll().stream()
                .map(dbClient -> new Client.ClientBuilder()
                        .withId(dbClient.getId())
                        .withFirstName(dbClient.getFirstName())
                        .withLastName(dbClient.getLastName())
                        .build())
                .collect(Collectors.toList());
    }
}
