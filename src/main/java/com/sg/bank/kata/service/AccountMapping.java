package com.sg.bank.kata.service;

import com.sg.bank.kata.model.Account;
import com.sg.bank.kata.model.Operation;
import com.sg.bank.kata.repository.model.DBAccount;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class AccountMapping {
    private final OperationMapping operationMapping;

    public AccountMapping(OperationMapping operationMapping){
        this.operationMapping = operationMapping;
    }

    public Account mapToAccount(DBAccount dbAccount){

       return new Account.AccountBuilder()
                .withNumber(dbAccount.getNumber())
                .withAccountBalance(dbAccount.getAccountBalance())
                .withOperations(mapToOperations(dbAccount))
                .build();
    }

    private List<Operation> mapToOperations(DBAccount dbAccount) {
        return dbAccount.getOperations().stream()
                .map(operationMapping::mapToOperation).collect(Collectors.toList());
    }
}
