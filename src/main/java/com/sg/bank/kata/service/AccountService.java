package com.sg.bank.kata.service;

import com.sg.bank.kata.controller.exception.NotFoundException;
import com.sg.bank.kata.model.Account;
import com.sg.bank.kata.model.Operation;
import com.sg.bank.kata.repository.AccountRepository;
import com.sg.bank.kata.repository.model.DBAccount;
import com.sg.bank.kata.repository.model.DBOperation;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AccountService {

    private final OperationMapping operationMapping;
    private final AccountRepository accountRepository;
    private final AccountMapping accountMapping;

    public AccountService(OperationMapping operationMapping,
                          AccountMapping accountMapping,
                          AccountRepository accountRepository) {
        this.operationMapping = operationMapping;
        this.accountRepository = accountRepository;
        this.accountMapping = accountMapping;
    }

    @Transactional
    public void saveOperation(long clientId, String accountCode, Operation operation) {

        DBAccount dbAccount = accountRepository.findByNumberAndClient_Id(accountCode, clientId)
                .orElseThrow(() -> new NotFoundException("Unknown account or client"));
        BigDecimal newAccountBalance = dbAccount.getAccountBalance().add(operation.getAmount());
        dbAccount.setAccountBalance(newAccountBalance);

        DBOperation dbOperation = operationMapping.mapToDBOperation(dbAccount, operation);
        dbAccount.getOperations().add(dbOperation);
        accountRepository.save(dbAccount);
    }

    public List<Account> findAllAccountsByClientId(long clientId) {

        List<DBAccount> dbAccounts = accountRepository.findByClient_Id(clientId);
        return dbAccounts.stream()
                .map(accountMapping::mapToAccount)
                .collect(Collectors.toList());
    }

    public Account findAllAccountsByClientIdAndAccountCode(long clientId, String number) {
        return accountRepository.findByNumberAndClient_Id(number, clientId)
                .map(accountMapping::mapToAccount)
                .orElseThrow(() -> new NotFoundException("Unknown account"));
    }


}
