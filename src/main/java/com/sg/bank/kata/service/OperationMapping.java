package com.sg.bank.kata.service;

import com.sg.bank.kata.model.Operation;
import com.sg.bank.kata.repository.model.DBAccount;
import com.sg.bank.kata.repository.model.DBOperation;
import org.springframework.stereotype.Component;

@Component
public class OperationMapping {

    public DBOperation mapToDBOperation(
            DBAccount dbAccount, Operation operation){
            DBOperation dbOperation = new DBOperation();
            dbOperation.setAccount(dbAccount);
            dbOperation.setAmount(operation.getAmount());
            dbOperation.setDescription(operation.getDescription());
            dbOperation.setType(operation.getType());
            dbOperation.setDate(operation.getDate());
            return dbOperation;
    }

    public Operation mapToOperation(DBOperation dbOperation){
        return new Operation.OperationBuilder()
                .withType(dbOperation.getType())
                .withDate(dbOperation.getDate())
                .withDescription(dbOperation.getDescription())
                .withAmount(dbOperation.getAmount())
                .build();
    }
}
