package com.sg.bank.kata.model;

public enum OperationTypeEnum {
    DEPOSIT,
    WITHDRAWAL
}
