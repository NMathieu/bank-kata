package com.sg.bank.kata.model;

import java.time.LocalDate;
import java.util.List;

public class Client {
    private final Long id;
    private final String firstName;
    private final String lastName;

    private Client(Long id, String firstName, String lastName){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public static class ClientBuilder{
        private Long id;
        private String firstName;
        private String lastName;

        public ClientBuilder(){
        }

        public ClientBuilder withId(Long id){
            this.id = id;
            return this;
        }
        public ClientBuilder withFirstName(String firstName){
            this.firstName = firstName;
            return this;
        }
        public  ClientBuilder withLastName(String lastName){
            this.lastName = lastName;
            return this;
        }


        public Client build(){
            return new Client(id, firstName, lastName);
        }
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
