package com.sg.bank.kata.model;

import java.math.BigDecimal;
import java.util.List;

public class Account {
    private final String number;
    private final List<Operation> operations;
    private final BigDecimal accountBalance;

    private Account(String number, List<Operation> operations, BigDecimal accountBalance) {
        this.number = number;
        this.operations = operations;
        this.accountBalance = accountBalance;
    }

    public static class AccountBuilder {
        private String number;
        private List<Operation> operations;
        private BigDecimal accountBalance;

        public AccountBuilder(){
        }

        public AccountBuilder withNumber(String number) {
            this.number = number;
            return this;
        }

        public AccountBuilder withOperations(List<Operation> operations) {
            this.operations = operations;
            return this;
        }

        public AccountBuilder withAccountBalance(BigDecimal accountBalance) {
            this.accountBalance = accountBalance;
            return this;
        }

        public Account build() {
            return new Account(number, operations, accountBalance);
        }
    }

    public String getNumber() {
        return number;
    }

    public List<Operation> getOperations() {
        return operations;
    }

    public BigDecimal getAccountBalance() {
        return accountBalance;
    }
}