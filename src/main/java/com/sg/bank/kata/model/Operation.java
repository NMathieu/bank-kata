package com.sg.bank.kata.model;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Operation {
    private LocalDate date;
    private BigDecimal amount;
    private String description;
    private OperationTypeEnum type;

    private Operation(LocalDate date, BigDecimal amount, String description, OperationTypeEnum type){
        this.date = date;
        this.amount = amount;
        this.description = description;
        this.type = type;
    }

    public static class OperationBuilder {
        private LocalDate date;
        private BigDecimal amount;
        private String description;
        private OperationTypeEnum type;

        public OperationBuilder(){
        }

        public OperationBuilder withDate(LocalDate date){
            this.date = date;
            return this;
        }

        public OperationBuilder withAmount(BigDecimal amount){
            this.amount = amount;
            return this;
        }
        public OperationBuilder withDescription(String description) {
            this.description = description;
            return this;
        }
        public OperationBuilder withType(OperationTypeEnum type){
            this.type = type;
            return this;
        }

        public Operation build(){
            return new Operation(date, amount, description, type);
        }
    }

    public LocalDate getDate() {
        return date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getDescription() {
        return description;
    }

    public OperationTypeEnum getType() {
        return type;
    }
}
