package com.sg.bank.kata;

import com.sg.bank.kata.repository.AccountRepository;
import com.sg.bank.kata.repository.ClientRepository;
import com.sg.bank.kata.repository.model.DBAccount;
import com.sg.bank.kata.repository.model.DBClient;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class DBInit implements CommandLineRunner {
    private final AccountRepository accountRepository;
    private final ClientRepository clientRepository;

    public DBInit(AccountRepository accountRepository, ClientRepository clientRepository) {
        this.accountRepository = accountRepository;
        this.clientRepository = clientRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        var defaultClient = new DBClient();
        defaultClient.setFirstName("Henri");
        defaultClient.setLastName("Dupond");

        defaultClient = clientRepository.save(defaultClient);

        var defaultAccount = new DBAccount();
        defaultAccount.setClient(defaultClient);
        defaultAccount.setAccountBalance(BigDecimal.valueOf(1000000));
        defaultAccount.setNumber("003002001");

        accountRepository.save(defaultAccount);
    }
}
