package com.sg.bank.kata.repository.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "ACCOUNT")
public class DBAccount {
    @Id
    private String number;

    @ManyToOne
    @JoinColumn(name = "client_id")
    private DBClient client;

    private BigDecimal accountBalance;

    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL)
    private List<DBOperation> operations = new ArrayList<>();

    public DBClient getClient() {
        return client;
    }

    public void setClient(DBClient client) {
        this.client = client;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public BigDecimal getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(BigDecimal accountBalance) {
        this.accountBalance = accountBalance;
    }

    public List<DBOperation> getOperations() {
        return operations;
    }

    public void setOperations(List<DBOperation> operations) {
        this.operations = operations;
    }
}
