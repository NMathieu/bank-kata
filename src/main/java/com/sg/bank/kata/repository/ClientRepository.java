package com.sg.bank.kata.repository;

import com.sg.bank.kata.repository.model.DBClient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<DBClient, Long> {
}
