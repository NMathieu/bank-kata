package com.sg.bank.kata.repository;

import com.sg.bank.kata.repository.model.DBAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<DBAccount, String> {

    Optional<DBAccount> findByNumberAndClient_Id(String number, long clientId);

    List<DBAccount> findByClient_Id(long clientId);
}
