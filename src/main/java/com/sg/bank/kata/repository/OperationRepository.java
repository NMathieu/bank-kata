package com.sg.bank.kata.repository;

import com.sg.bank.kata.repository.model.DBOperation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OperationRepository extends JpaRepository<DBOperation, Long> {
}
