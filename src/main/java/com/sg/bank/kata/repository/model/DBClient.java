package com.sg.bank.kata.repository.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "CLIENT")
public class DBClient {
    @Id
    @GeneratedValue
    private long id;
    private String firstName;
    private String lastName;
    @OneToMany(mappedBy = "client", cascade = CascadeType.ALL)
    private List<DBAccount> accounts = new ArrayList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<DBAccount> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<DBAccount> accounts) {
        this.accounts = accounts;
    }
}
