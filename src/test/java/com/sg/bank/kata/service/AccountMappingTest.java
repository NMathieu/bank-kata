package com.sg.bank.kata.service;

import com.sg.bank.kata.model.Account;
import com.sg.bank.kata.repository.model.DBAccount;
import com.sg.bank.kata.repository.model.DBClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.math.BigDecimal;
import java.util.Collections;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

public class AccountMappingTest {

    private AccountMapping accountMapping;

    @BeforeEach
    public void init(){
        OperationMapping operationMapping = new OperationMapping();
        accountMapping = new AccountMapping(operationMapping);
    }


    @Test
    public void mapToAccount(){
        DBAccount dbAccount = createDBAccount("2344", createDBClient(), new BigDecimal("12"));
        Account account = accountMapping.mapToAccount(dbAccount);

        assertAll(
                () -> assertThat(account.getNumber()).isEqualTo(dbAccount.getNumber()),
                () -> assertThat(account.getAccountBalance()).isEqualTo(dbAccount.getAccountBalance()),
                () -> assertThat(account.getOperations().size()).isEqualTo(dbAccount.getOperations().size())
        );
    }

    private DBClient createDBClient(){
        DBClient dbClient = new DBClient();
        dbClient.setId(123L);
        dbClient.setFirstName("Test1");
        dbClient.setLastName("Test2");
        return dbClient;
    }

    private DBAccount createDBAccount(String number, DBClient dbClient,
                                      BigDecimal accountBalance){
        DBAccount dbAccount  = new DBAccount();
        dbAccount.setNumber(number);
        dbAccount.setClient(dbClient);
        dbAccount.setAccountBalance(accountBalance);
        dbAccount.setOperations(Collections.emptyList());
        return dbAccount;
    }


}
