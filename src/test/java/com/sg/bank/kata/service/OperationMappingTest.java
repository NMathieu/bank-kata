package com.sg.bank.kata.service;

import com.sg.bank.kata.model.Operation;
import com.sg.bank.kata.model.OperationTypeEnum;
import com.sg.bank.kata.repository.model.DBAccount;
import com.sg.bank.kata.repository.model.DBClient;
import com.sg.bank.kata.repository.model.DBOperation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

public class OperationMappingTest {
    OperationMapping operationMapping = null;

    @BeforeEach
    public void init(){
        operationMapping = new OperationMapping();
    }

    @Test
    public void mapOperationsReturnDBOperations() {

        DBAccount dbAccount = createDBAccount();
        Operation operation = createOperation(BigDecimal.TEN, "nouveau dépôt", OperationTypeEnum.DEPOSIT);

        DBOperation result = operationMapping.mapToDBOperation(dbAccount, operation);

        assertAll(
                () -> assertThat(result.getAmount().doubleValue()).isEqualTo(operation.getAmount().doubleValue()),
                () -> assertThat(result.getType()).isEqualTo(operation.getType()),
                () -> assertThat(result.getDescription()).isEqualTo(operation.getDescription()),
                () -> assertThat(result.getAccount().getNumber()).isEqualTo(dbAccount.getNumber()),
                () -> assertThat(result.getDate()).isEqualTo(operation.getDate())
        );
    }

    private DBAccount createDBAccount() {
        DBAccount dbAccount = new DBAccount();
        dbAccount.setNumber("123LO");
        dbAccount.setClient(new DBClient());
        dbAccount.setAccountBalance(BigDecimal.TEN);

        return dbAccount;
    }

    private Operation createOperation(BigDecimal amount, String description, OperationTypeEnum type) {
        return new Operation.
                OperationBuilder()
                .withAmount(amount)
                .withDate(LocalDate.now())
                .withDescription(description)
                .withType(type)
                .build();
    }

    @Test
    public void mapToOperation(){

        DBOperation dbOperation = createOperation(BigDecimal.ONE, "test",
                OperationTypeEnum.DEPOSIT,LocalDate.now(), "123", 455L);

        Operation operation = operationMapping.mapToOperation(dbOperation);
        assertAll(
                () -> assertThat(operation.getDate()).isEqualTo(dbOperation.getDate()),
                () -> assertThat(operation.getType()).isEqualTo(dbOperation.getType()),
                () -> assertThat(operation.getDescription()).isEqualTo(dbOperation.getDescription()),
                () -> assertThat(operation.getAmount()).isEqualTo(dbOperation.getAmount())
        );
    }

    private DBOperation createOperation(BigDecimal amount, String description, OperationTypeEnum type,
                                        LocalDate date, String number, Long id) {
        DBOperation dbOperation = new DBOperation();
        dbOperation.setDate(date);
        dbOperation.setDescription(description);
        dbOperation.setType(type);
        dbOperation.setAmount(amount);
        DBAccount dbAccount = new DBAccount();
        dbAccount.setNumber(number);
        dbOperation.setAccount(dbAccount);
        dbOperation.setId(id);

        return dbOperation;
    }
}
