package com.sg.bank.kata.service;

import com.sg.bank.kata.controller.exception.NotFoundException;
import com.sg.bank.kata.model.Account;
import com.sg.bank.kata.repository.AccountRepository;
import com.sg.bank.kata.repository.model.DBAccount;
import com.sg.bank.kata.repository.model.DBClient;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class AccountServiceTest {

    public static final long CLIENT_ID = 123L;
    public static final String ACCOUNT_CODE = "ACC1223";
    private final AccountRepository accountRepository = mock(AccountRepository.class);
    private final OperationMapping operationMapping = mock(OperationMapping.class);
    private final AccountMapping accountMapping = mock(AccountMapping.class);

    private final AccountService accountService = new AccountService(operationMapping, accountMapping, accountRepository);

    @Test
    public void findAllAccountsByClientIdReturnAnEmptyList() {
        List<DBAccount> dbAccounts = Collections.emptyList();
        Mockito.when(accountRepository.findByClient_Id(CLIENT_ID))
                .thenReturn(dbAccounts);

        List<Account> accounts = accountService.findAllAccountsByClientId(CLIENT_ID);
        assertThat(accounts.size()).isEqualTo(0);
    }

    @Test
    public void findAllAccountsByClientIdReturnAccounts() {
        DBAccount dbAccount = createDBAccount("2344", createDBClient(), new BigDecimal("12"));
        List<DBAccount> dbAccounts = Collections.singletonList(dbAccount);

        Mockito.when(accountRepository.findByClient_Id(CLIENT_ID))
                .thenReturn(dbAccounts);

        List<Account> accounts = accountService.findAllAccountsByClientId(CLIENT_ID);
        assertThat(accounts.size()).isEqualTo(dbAccounts.size());
    }


    @Test
    public void findAllAccountsByClientIdAndAccountCodeReturnNotFoundException() {
        when(accountRepository.findByNumberAndClient_Id(ACCOUNT_CODE, CLIENT_ID)).thenReturn(Optional.empty());
        assertThrows(
                NotFoundException.class, () -> {
                    accountService.findAllAccountsByClientIdAndAccountCode(CLIENT_ID, ACCOUNT_CODE);
                }
        );
    }

    private DBAccount createDBAccount(String number, DBClient dbClient,
                                      BigDecimal accountBalance) {
        DBAccount dbAccount = new DBAccount();
        dbAccount.setNumber(number);
        dbAccount.setClient(dbClient);
        dbAccount.setAccountBalance(accountBalance);
        dbAccount.setOperations(Collections.emptyList());
        return dbAccount;
    }

    private DBClient createDBClient() {
        DBClient dbClient = new DBClient();
        dbClient.setId(CLIENT_ID);
        dbClient.setFirstName("Test1");
        dbClient.setLastName("Test2");
        return dbClient;
    }

}
