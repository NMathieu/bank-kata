package com.sg.bank.kata.controller;

import com.sg.bank.kata.controller.api.BankOperationRequest;
import com.sg.bank.kata.controller.exception.BadRequestException;
import com.sg.bank.kata.model.Operation;
import com.sg.bank.kata.model.OperationTypeEnum;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

public class BankOperationMappingTest {
    private static BankOperationMapping mapping = null;

    @BeforeEach
    public void init(){
         mapping = new BankOperationMapping();
    }

    @Test
    public void mapToModelReturnBadRequestExceptionWhenDepositIsNegate(){

        BankOperationRequest bankOperationRequest = new BankOperationRequest(new BigDecimal("-1"),"depot en liquide", OperationTypeEnum.DEPOSIT );
         Assertions.assertThrows(BadRequestException.class, () -> {
             mapping.mapToOperation(bankOperationRequest);
         });
    }

    @Test
    public void mapToModelReturnBadRequestExceptionWhenDepositIsPositive(){

        BankOperationRequest bankOperationRequest = new BankOperationRequest(new BigDecimal("56"),"depot en liquide", OperationTypeEnum.WITHDRAWAL );
        Assertions.assertThrows(BadRequestException.class, () -> {
            mapping.mapToOperation(bankOperationRequest);
        });
    }

    @Test
    public void mapToModelReturnAccount(){

        BankOperationRequest bankOperationRequest = new BankOperationRequest( new BigDecimal("12.09"),"depot en liquide", OperationTypeEnum.DEPOSIT );

        Operation operation = mapping.mapToOperation(bankOperationRequest);

        Assertions.assertAll(
                () -> assertThat(operation).isNotNull(),
                () -> assertThat(operation.getType()).isEqualTo(bankOperationRequest.getOperationType()),
                () -> assertThat(operation.getAmount().doubleValue()).isEqualTo(bankOperationRequest.getAmount().doubleValue()),
                () -> assertThat(operation.getDescription()).isEqualTo(bankOperationRequest.getDescription()),
                () -> assertThat(operation.getDate()).isEqualTo(LocalDate.now())
        );
    }
}
